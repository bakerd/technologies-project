//Pacakages needed
const bodyParser = require("body-parser");
const path = require("path");
const express = require("express");


//Server
var server = express();
server.set("view engine", "pug");
server.set("views", path.join(__dirname, "views"));
const http = require("http");
const hostname = "127.0.0.1";
const port = 3000;

//var pageDir = require(path.join(__dirname, "views"));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: false}));
server.use(express.static(path.join(__dirname, 'public')));

//Pages
//Index - IP Location Lookup
server.get("/", function(req, res) {
    res.render ("index", {
        title: "Home Page"
    })
})

//Weather


//Listener
server.listen(port, hostname, () => {
    console.log("Server started on port " + port);
})