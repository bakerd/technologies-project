$(document).ready(function() {
    $("#GetIpInfo").click(function () {
        var ipAddress = $("#txtIP").val();
        var url = "http://www.geoplugin.net/json.gp?ip=";
        
        $.get (url.concat(ipAddress), function(data){
            $("#ipAdd").append(data.geoplugin_request);    
            $("#gCity").append(data.geoplugin_city);    
            $("#gRegion").append(data.geoplugin_region);    
            $("#gCountry").append(data.geoplugin_countryName);    
            $("#gContinent").append(data.geoplugin_continentName);    
            $("#gLat").append(data.geoplugin_latitude);    
            $("#gLong").append(data.geoplugin_longitude);    
            $("#gTimezone").append(data.geoplugin_timezone);    
            $("#results").show(1000);
       }, "json")
    })

    $("#Clear").click(function () {
        $("#results").hide(300);
        $("#ipAdd").html("IP Address: ");    
        $("#gCity").html("City: ");    
        $("#gRegion").html("Region: ");    
        $("#gCountry").html("Country: ");    
        $("#gContinent").html("Continent: ");    
        $("#gLat").html("Latitude: ");    
        $("#gLong").html("Longitude: ");    
        $("#gTimezone").html("Timezone: "); 
        $("#txtIP").val("");
    })
})
